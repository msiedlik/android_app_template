package pl.siedlik.androidapptemplate.base.ui.view

import android.content.Context
import android.util.AttributeSet
import androidx.core.content.ContextCompat
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import pl.siedlik.androidapptemplate.R

class ColoredSwipeRefreshLayout : SwipeRefreshLayout {

  constructor(context: Context) : super(context) {
    initialize()
  }

  constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
    initialize()
  }

  private fun initialize() {
    setColorSchemeColors(ContextCompat.getColor(context, R.color.colorAccent))
  }
}