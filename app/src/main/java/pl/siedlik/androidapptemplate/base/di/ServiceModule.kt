package pl.siedlik.androidapptemplate.base.di

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import pl.siedlik.androidapptemplate.feature.posts.network.PostsService
import retrofit2.Retrofit

object ServiceModule {

  val module = Kodein.Module("serviceModule") {
    bind<PostsService>() with singleton { instance<Retrofit>().create(PostsService::class.java) }
  }
}