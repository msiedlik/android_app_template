package pl.siedlik.androidapptemplate.base.app

import android.app.Application
import com.jakewharton.threetenabp.AndroidThreeTen
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import pl.siedlik.androidapptemplate.BuildConfig
import pl.siedlik.androidapptemplate.base.di.RepositoryModule
import pl.siedlik.androidapptemplate.base.di.ServiceModule
import pl.siedlik.androidapptemplate.base.di.ViewModelModule
import pl.siedlik.androidapptemplate.base.network.di.NetworkModule
import timber.log.Timber
import timber.log.Timber.DebugTree

class App : Application(), KodeinAware {

  override val kodein by Kodein.lazy {
    import(androidXModule(this@App))
    import(NetworkModule.module)
    import(ServiceModule.module)
    import(RepositoryModule.module)
    import(ViewModelModule.module)
  }

  override fun onCreate() {
    super.onCreate()
    initializeDateTime()
    initializeTimber()
  }

  private fun initializeDateTime() {
    AndroidThreeTen.init(this)
  }

  private fun initializeTimber() {
    if (BuildConfig.DEBUG) {
      Timber.plant(DebugTree())
    }
  }
}