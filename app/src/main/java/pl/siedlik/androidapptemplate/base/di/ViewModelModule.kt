package pl.siedlik.androidapptemplate.base.di

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton
import pl.siedlik.androidapptemplate.base.extensions.bindViewModel
import pl.siedlik.androidapptemplate.base.ui.viewmodel.ViewModelFactory
import pl.siedlik.androidapptemplate.feature.posts.ui.PostsViewModel

object ViewModelModule {

  val module = Kodein.Module("viewModelModule") {
    bind<ViewModelFactory>() with singleton { ViewModelFactory(kodein) }
    bindViewModel<PostsViewModel>() with provider { PostsViewModel(instance()) }
  }
}