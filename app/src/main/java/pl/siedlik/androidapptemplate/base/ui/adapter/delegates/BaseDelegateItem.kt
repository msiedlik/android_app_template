package pl.siedlik.androidapptemplate.base.ui.adapter.delegates

interface BaseDelegateItem {

  val id: Any

  fun generateId(): String = "${javaClass.simpleName}$id"
}