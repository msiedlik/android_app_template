package pl.siedlik.androidapptemplate.feature.posts.network

import pl.siedlik.androidapptemplate.feature.posts.model.Post
import retrofit2.Response
import retrofit2.http.GET

interface PostsService {

  @GET("posts")
  suspend fun getPosts(): Response<List<Post>>
}