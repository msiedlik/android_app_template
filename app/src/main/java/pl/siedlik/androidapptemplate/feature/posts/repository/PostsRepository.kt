package pl.siedlik.androidapptemplate.feature.posts.repository

import pl.siedlik.androidapptemplate.feature.posts.model.Post
import retrofit2.Response

interface PostsRepository {

  suspend fun getPosts(): Response<List<Post>>
}