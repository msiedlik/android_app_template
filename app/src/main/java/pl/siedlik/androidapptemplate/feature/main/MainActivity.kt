package pl.siedlik.androidapptemplate.feature.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupActionBarWithNavController
import pl.siedlik.androidapptemplate.R
import pl.siedlik.androidapptemplate.R.layout

class MainActivity : AppCompatActivity() {

  private lateinit var navController: NavController

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(layout.activity_main)
    navController = Navigation.findNavController(this, R.id.navHostFragment)
    setupActionBarWithNavController(navController)
  }

  override fun onSupportNavigateUp() = NavigationUI.navigateUp(navController, null)
}
