package pl.siedlik.androidapptemplate.feature.posts.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_posts.errorView
import kotlinx.android.synthetic.main.fragment_posts.postsRecyclerView
import kotlinx.android.synthetic.main.fragment_posts.refreshLayout
import pl.siedlik.androidapptemplate.R
import pl.siedlik.androidapptemplate.base.network.error.NetworkErrorUtils
import pl.siedlik.androidapptemplate.base.ui.fragment.KodeinFragment
import pl.siedlik.androidapptemplate.base.ui.livedata.FetchState
import pl.siedlik.androidapptemplate.base.ui.livedata.FetchState.Error
import pl.siedlik.androidapptemplate.base.ui.livedata.FetchState.Progress
import pl.siedlik.androidapptemplate.base.ui.livedata.FetchState.Success
import pl.siedlik.androidapptemplate.feature.posts.model.Post
import pl.siedlik.androidapptemplate.feature.posts.ui.adapter.PostsAdapter

class PostsFragment : KodeinFragment() {

  private val viewModel: PostsViewModel by viewModel()

  private val postsAdapter by lazy { PostsAdapter(::onPostsClicked) }

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? = inflater.inflate(R.layout.fragment_posts, container, false)

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    setupSwipeToRefresh()
    setupRecycler()
    observePosts()
  }

  private fun observePosts() {
    viewModel.posts.observeBy(::renderPosts)
  }

  private fun renderPosts(state: FetchState<List<Post>>) {
    when (state) {
      is Progress -> {
        refreshLayout.isRefreshing = true
      }
      is Success -> {
        refreshLayout.isRefreshing = false
        errorView.isVisible = false
        postsRecyclerView.isVisible = true
        postsAdapter.submitList(state.result)
      }
      is Error -> {
        refreshLayout.isRefreshing = false
        postsRecyclerView.isVisible = false
        errorView.showAndSetMessage(NetworkErrorUtils.getErrorViewType(state.cause))
      }
    }
  }

  private fun onPostsClicked(id: Int) {
    Snackbar.make(requireView(), "Pressed post id: $id", Snackbar.LENGTH_SHORT).show()
  }

  private fun setupSwipeToRefresh() {
    refreshLayout.setOnRefreshListener { viewModel.refresh() }
  }

  private fun setupRecycler() {
    postsRecyclerView.run {
      layoutManager = LinearLayoutManager(requireContext())
      adapter = postsAdapter
      layoutAnimation = AnimationUtils.loadLayoutAnimation(requireContext(), R.anim.layout_animation_fall_down)
    }
  }
}