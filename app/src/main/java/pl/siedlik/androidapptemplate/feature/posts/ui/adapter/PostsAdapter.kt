package pl.siedlik.androidapptemplate.feature.posts.ui.adapter

import pl.siedlik.androidapptemplate.base.ui.adapter.delegates.BaseListAdapter

class PostsAdapter(
  onItemClicked: (Int) -> Unit
) : BaseListAdapter(
  PostsDelegate(onItemClicked)
)