package pl.siedlik.androidapptemplate.feature.posts.model

import pl.siedlik.androidapptemplate.base.ui.adapter.delegates.BaseDelegateItem

data class Post(
  override val id: Int,
  val userId: Int,
  val title: String,
  val body: String
) : BaseDelegateItem