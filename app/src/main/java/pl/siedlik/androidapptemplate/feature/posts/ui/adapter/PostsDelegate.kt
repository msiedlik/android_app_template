package pl.siedlik.androidapptemplate.feature.posts.ui.adapter

import androidx.recyclerview.widget.RecyclerView.ViewHolder
import kotlinx.android.synthetic.main.item_post.view.postBody
import kotlinx.android.synthetic.main.item_post.view.postTitle
import kotlinx.android.synthetic.main.item_post.view.userId
import pl.siedlik.androidapptemplate.R
import pl.siedlik.androidapptemplate.base.ui.adapter.delegates.BaseAdapterDelegate
import pl.siedlik.androidapptemplate.feature.posts.model.Post

class PostsDelegate(
  private val onItemClicked: (Int) -> Unit
) : BaseAdapterDelegate<Post>(Post::class, R.layout.item_post) {

  override fun bind(item: Post, holder: ViewHolder) {
    with(holder.itemView) {
      userId.text = item.id.toString()
      postTitle.text = item.title
      postBody.text = item.body
      setOnClickListener { onItemClicked(item.id) }
    }
  }
}