package pl.siedlik.androidapptemplate.feature.posts.repository

import pl.siedlik.androidapptemplate.feature.posts.model.Post
import pl.siedlik.androidapptemplate.feature.posts.network.PostsService
import retrofit2.Response

class PostsRepositoryImpl(
  private val service: PostsService
) : PostsRepository {

  override suspend fun getPosts(): Response<List<Post>> =
    service.getPosts()
}